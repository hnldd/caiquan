<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * fight
 * @author crud自动生成代码
 * @date 2023/12/22 10:24:45
 */

namespace app\services\crud;

use app\services\BaseServices;
use think\exception\ValidateException;
use app\dao\crud\GameFightDao;
use crmeb\services\FormBuilder;

/**
 * Class CrudService
 * @date 2023/12/22
 * @package app\services\crud
 */
class GameFightServices extends BaseServices
{

    /**
     * GameFightServices constructor.
     * @param GameFightDao $dao
     */
    public function __construct(GameFightDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * 主页数据接口
     * @param array $where
     * @return array
     * @date 2023/12/22
     */
    public function getCrudListIndex(array $where = [])
    {
        [$page, $limit] = $this->getPageValue();
        $model = $this->dao->searchCrudModel($where, 'name,fight_sn,user_a,user_b,victory_a,victory_b,draw,status,id', 'id desc', ['userAHasOne','userBHasOne']);

        return ['count' => $model->count(), 'list' => $model->page($page ?: 1, $limit ?: 10)->select()->toArray()];
    }

    /**
     * 编辑和获取表单
     * @date 2023/12/22
     * @param int $id
     * @return array
     */
    public function getCrudForm(int $id = 0)
    {
        $url = '/crud/game_fight';
        $info = [];
        if ($id) {
            $info = $this->dao->get($id);
            if (!$info) {
                throw new ValidateException(100026);
            }
            $url .= '/' . $id;
        }
        $rule = [];

        $rule[] = FormBuilder::input("name", "对局名称",  $info["name"] ?? '');
        $rule[] = FormBuilder::input("fight_sn", "对局编号",  $info["fight_sn"] ?? '');
        $rule[] = FormBuilder::input("user_a", "用户A",  $info["user_a"] ?? '');
        $rule[] = FormBuilder::input("user_b", "用户B",  $info["user_b"] ?? '');
        $rule[] = FormBuilder::number("victory_a", "A胜利",  $info["victory_a"] ?? '');
        $rule[] = FormBuilder::number("victory_b", "B胜利",  $info["victory_b"] ?? '');
        $rule[] = FormBuilder::number("draw", "平局",  $info["draw"] ?? '');
        $rule[] = FormBuilder::number("status", "状态",  $info["status"] ?? '');

        return create_form('fight', $rule, $url, $id ? 'PUT' : 'POST');
    }

    /**
     * 新增
     * @date 2023/12/22
     * @param array $data
     * @return mixed
     */
    public function crudSave(array $data)
    {
        return $this->dao->save($data);
    }

    /**
     * 修改
     * @date 2023/12/22
     * @param int $id
     * @param array $data
     * @return \crmeb\basic\BaseModel
     */
    public function crudUpdate(int $id, array $data)
    {
        return $this->dao->update($id, $data);
    }
    //获取当前对战状态
    public function getFightStatus(){
        return $this->dao->getFightStatus();
    }
    //创建对战
    public function createFight($uid){
        return $this->dao->createFight($uid);
    }
    public function getFightRow($fightSn){
        return $this->dao->getFightRow($fightSn);
    }
    public function endFightRow($fightSn){
        return $this->dao->endFightRow($fightSn);
    }

}
