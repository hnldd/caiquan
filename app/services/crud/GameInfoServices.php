<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * game_info
 * @author crud自动生成代码
 * @date 2023/12/26 09:32:22
 */

namespace app\services\crud;

use app\services\BaseServices;
use think\exception\ValidateException;
use app\dao\crud\GameInfoDao;
use crmeb\services\FormBuilder;

/**
 * Class CrudService
 * @date 2023/12/26
 * @package app\services\crud
 */
class GameInfoServices extends BaseServices
{

    /**
     * GameInfoServices constructor.
     * @param GameInfoDao $dao
     */
    public function __construct(GameInfoDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * 主页数据接口
     * @param array $where
     * @return array
     * @date 2023/12/26
     */
    public function getCrudListIndex(array $where = [])
    {
        [$page, $limit] = $this->getPageValue();
        $model = $this->dao->searchCrudModel($where, 'fight_sn,user_id,watchword,gesture,round,result,id', 'id desc', []);

        return ['count' => $model->count(), 'list' => $model->page($page ?: 1, $limit ?: 10)->select()->toArray()];
    }

    /**
     * 编辑和获取表单
     * @date 2023/12/26
     * @param int $id
     * @return array
     */
    public function getCrudForm(int $id = 0)
    {
        $url = '/crud/game_info';
        $info = [];
        if ($id) {
            $info = $this->dao->get($id);
            if (!$info) {
                throw new ValidateException(100026);
            }
            $url .= '/' . $id;
        }
        $rule = [];

        $rule[] = FormBuilder::input("fight_sn", "对局编号",  $info["fight_sn"] ?? '');
        $rule[] = FormBuilder::number("user_id", "用户id",  $info["user_id"] ?? '');
        $rule[] = FormBuilder::number("watchword", "口令",  $info["watchword"] ?? '');
        $rule[] = FormBuilder::number("gesture", "手势",  $info["gesture"] ?? '');
        $rule[] = FormBuilder::number("round", "回合",  $info["round"] ?? '');
        $rule[] = FormBuilder::number("result", "结果",  $info["result"] ?? '');

        return create_form('game_info', $rule, $url, $id ? 'PUT' : 'POST');
    }

    /**
     * 新增
     * @date 2023/12/26
     * @param array $data
     * @return mixed
     */
    public function crudSave(array $data)
    {
        return $this->dao->save($data);
    }

    /**
     * 修改
     * @date 2023/12/26
     * @param int $id
     * @param array $data
     * @return \crmeb\basic\BaseModel
     */
    public function crudUpdate(int $id, array $data)
    {
        return $this->dao->update($id, $data);
    }
    //出拳
    public function saveInfo($infoData){
        return $this->dao->saveInfo($infoData);
    }
    //判输赢
    public function checkResult($fightSn){
        return $this->dao->checkResult($fightSn);
    }
    public function getInfoRow($fightSn,$userId,$round){
        return $this->dao->getInfoRow($fightSn,$userId,$round);
    }
}
