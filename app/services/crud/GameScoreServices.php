<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * game_score
 * @author crud自动生成代码
 * @date 2023/12/22 10:45:44
 */

namespace app\services\crud;

use app\services\BaseServices;
use think\exception\ValidateException;
use app\dao\crud\GameScoreDao;
use crmeb\services\FormBuilder;

/**
 * Class CrudService
 * @date 2023/12/22
 * @package app\services\crud
 */
class GameScoreServices extends BaseServices
{

    /**
     * GameScoreServices constructor.
     * @param GameScoreDao $dao
     */
    public function __construct(GameScoreDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * 主页数据接口
     * @param array $where
     * @return array
     * @date 2023/12/22
     */
    public function getCrudListIndex(array $where = [])
    {
        [$page, $limit] = $this->getPageValue();
        $model = $this->dao->searchCrudModel($where, 'user_id,day,month,year,id', 'id desc', ['userIdHasOne']);

        return ['count' => $model->count(), 'list' => $model->page($page ?: 1, $limit ?: 10)->select()->toArray()];
    }

    /**
     * 编辑和获取表单
     * @date 2023/12/22
     * @param int $id
     * @return array
     */
    public function getCrudForm(int $id = 0)
    {
        $url = '/crud/game_score';
        $info = [];
        if ($id) {
            $info = $this->dao->get($id);
            if (!$info) {
                throw new ValidateException(100026);
            }
            $url .= '/' . $id;
        }
        $rule = [];

        $rule[] = FormBuilder::number("user_id", "用户id",  $info["user_id"] ?? '');
        $rule[] = FormBuilder::number("day", "日积分",  $info["day"] ?? '');
        $rule[] = FormBuilder::number("month", "月积分",  $info["month"] ?? '');
        $rule[] = FormBuilder::number("year", "年积分",  $info["year"] ?? '');

        return create_form('game_score', $rule, $url, $id ? 'PUT' : 'POST');
    }

    /**
     * 新增
     * @date 2023/12/22
     * @param array $data
     * @return mixed
     */
    public function crudSave(array $data)
    {
        return $this->dao->save($data);
    }

    /**
     * 修改
     * @date 2023/12/22
     * @param int $id
     * @param array $data
     * @return \crmeb\basic\BaseModel
     */
    public function crudUpdate(int $id, array $data)
    {
        return $this->dao->update($id, $data);
    }

    //获取用户游戏积分信息
    public function getUserSroce($uid){
        return $this->dao->getUserScore($uid);
    }
    public function saveVictoryScore($uid){
        return $this->dao->saveVictoryScore($uid);
    }
    public function getMaxScore(){
        return $this->dao->getMaxScore();
    }
}
