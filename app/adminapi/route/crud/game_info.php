<?php


use think\facade\Route;

Route::get('crud/game_info', 'crud.GameInfo/index')->option(['real_name' => 'game_info列表接口']);

Route::get('crud/game_info/create', 'crud.GameInfo/create')->option(['real_name' => 'game_info获取创建表单接口']);

Route::post('crud/game_info', 'crud.GameInfo/save')->option(['real_name' => 'game_info保存接口']);

Route::get('crud/game_info/:id/edit', 'crud.GameInfo/edit')->option(['real_name' => 'game_info获取修改表单接口']);

Route::put('crud/game_info/:id', 'crud.GameInfo/update')->option(['real_name' => 'game_info修改接口']);

Route::put('crud/game_info/status/:id', 'crud.GameInfo/status')->option(['real_name' => 'game_info修改状态接口']);

Route::delete('crud/game_info/:id', 'crud.GameInfo/delete')->option(['real_name' => 'game_info删除接口']);

Route::get('crud/game_info/:id', 'crud.GameInfo/read')->option(['real_name' => 'game_info查看接口']);



