<?php


use think\facade\Route;

Route::get('crud/game_score', 'crud.GameScore/index')->option(['real_name' => 'game_score列表接口']);

Route::get('crud/game_score/create', 'crud.GameScore/create')->option(['real_name' => 'game_score获取创建表单接口']);

Route::post('crud/game_score', 'crud.GameScore/save')->option(['real_name' => 'game_score保存接口']);

Route::get('crud/game_score/:id/edit', 'crud.GameScore/edit')->option(['real_name' => 'game_score获取修改表单接口']);

Route::put('crud/game_score/:id', 'crud.GameScore/update')->option(['real_name' => 'game_score修改接口']);

Route::put('crud/game_score/status/:id', 'crud.GameScore/status')->option(['real_name' => 'game_score修改状态接口']);

Route::delete('crud/game_score/:id', 'crud.GameScore/delete')->option(['real_name' => 'game_score删除接口']);

Route::get('crud/game_score/:id', 'crud.GameScore/read')->option(['real_name' => 'game_score查看接口']);



