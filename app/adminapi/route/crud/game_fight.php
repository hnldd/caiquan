<?php


use think\facade\Route;

Route::get('crud/game_fight', 'crud.GameFight/index')->option(['real_name' => 'fight列表接口']);

Route::get('crud/game_fight/create', 'crud.GameFight/create')->option(['real_name' => 'fight获取创建表单接口']);

Route::post('crud/game_fight', 'crud.GameFight/save')->option(['real_name' => 'fight保存接口']);

Route::get('crud/game_fight/:id/edit', 'crud.GameFight/edit')->option(['real_name' => 'fight获取修改表单接口']);

Route::put('crud/game_fight/:id', 'crud.GameFight/update')->option(['real_name' => 'fight修改接口']);

Route::put('crud/game_fight/status/:id', 'crud.GameFight/status')->option(['real_name' => 'fight修改状态接口']);

Route::delete('crud/game_fight/:id', 'crud.GameFight/delete')->option(['real_name' => 'fight删除接口']);

Route::get('crud/game_fight/:id', 'crud.GameFight/read')->option(['real_name' => 'fight查看接口']);



