<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * game_info
 * @author crud自动生成代码
 * @date 2023/12/26 09:32:22
 */

namespace app\adminapi\validate\crud;


use think\Validate;

/**
 * Class CrudValidate
 * @date 2023/12/26
 * @package app\adminapi\validate\crud
 */
class GameInfoValidate extends Validate
{

    /**
     * @var array
     */
    protected $rule = [

    ];

    /**
     * @var array
     */
    protected $message = [

    ];

    /**
     * @var array
     */
    protected $scene = [

    ];
}
