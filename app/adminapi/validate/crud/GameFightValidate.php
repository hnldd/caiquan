<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * fight
 * @author crud自动生成代码
 * @date 2023/12/22 10:24:45
 */

namespace app\adminapi\validate\crud;


use think\Validate;

/**
 * Class CrudValidate
 * @date 2023/12/22
 * @package app\adminapi\validate\crud
 */
class GameFightValidate extends Validate
{

    /**
     * @var array
     */
    protected $rule = [

    ];

    /**
     * @var array
     */
    protected $message = [

    ];

    /**
     * @var array
     */
    protected $scene = [

    ];
}
