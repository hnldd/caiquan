<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * game_info
 * @author crud自动生成代码
 * @date 2023/12/26 09:32:22
 */

namespace app\model\crud;


use crmeb\basic\BaseModel;

/**
 * Class GameInfo
 * @date 2023/12/26
 * @package app\model\crud
 */
class GameInfo extends BaseModel
{

    /**
     * 表名
     * @var string
     */
    protected $name = 'game_info';

    /**
     * 主键
     * @var string
     */
    protected $pk = 'id';

}
