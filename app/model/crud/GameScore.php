<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * game_score
 * @author crud自动生成代码
 * @date 2023/12/22 10:45:44
 */

namespace app\model\crud;


use crmeb\basic\BaseModel;

/**
 * Class GameScore
 * @date 2023/12/22
 * @package app\model\crud
 */
class GameScore extends BaseModel
{

    /**
     * 表名
     * @var string
     */
    protected $name = 'game_score';

    /**
     * 主键
     * @var string
     */
    protected $pk = 'id';


    /**
     * 用户id一对一关联
     * @date 2023/12/22
     * @return \think\model\relation\HasOne
     */
    public function userIdHasOne()
    {
        return $this->hasOne(\app\model\wechat\WechatUser::class, 'uid', 'user_id');
    }

}
