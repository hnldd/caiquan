<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * fight
 * @author crud自动生成代码
 * @date 2023/12/22 10:24:45
 */

namespace app\model\crud;


use crmeb\basic\BaseModel;

/**
 * Class GameFight
 * @date 2023/12/22
 * @package app\model\crud
 */
class GameFight extends BaseModel
{

    /**
     * 表名
     * @var string
     */
    protected $name = 'game_fight';

    /**
     * 主键
     * @var string
     */
    protected $pk = 'id';


    /**
     * 用户A一对一关联
     * @date 2023/12/22
     * @return \think\model\relation\HasOne
     */
    public function userAHasOne()
    {
        return $this->hasOne(\app\model\wechat\WechatUser::class, 'uid', 'user_a');
    }

    /**
     * 用户B一对一关联
     * @date 2023/12/22
     * @return \think\model\relation\HasOne
     */
    public function userBHasOne()
    {
        return $this->hasOne(\app\model\wechat\WechatUser::class, 'uid', 'user_b');
    }

}
