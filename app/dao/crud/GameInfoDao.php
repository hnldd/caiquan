<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * game_info
 * @author crud自动生成代码
 * @date 2023/12/26 09:32:22
 */

namespace app\dao\crud;


use app\dao\BaseDao;
use app\model\crud\GameInfo;
use app\dao\crud\GameFightDao;
use app\dao\crud\GameScoreDao;
/**
 * Class GameInfoDao
 * @date 2023/12/26
 * @package app\dao\crud
 */
class GameInfoDao extends BaseDao
{

    /**
     * 设置模型
     * @return string
     * @date 2023/12/26
     */
    protected function setModel(): string
    {
        return GameInfo::class;
    }
    /**
     * 搜索
     * @param array $where
     * @return \crmeb\basic\BaseModel
     * @throws \ReflectionException
     * @date {%DATE%}
     */
    public function searchCrudModel(array $where = [], $field = ['*'], string $order = '', array $with = [])
    {
        return $this->getModel()->field($field)->when($order !== '', function ($query) use ($order) {
            $query->order($order);
        })->when($with, function ($query) use ($with) {
            $query->with($with);
        });
    }
    public function getInfoResult($fightSn,$uid){
        $where=[['fight_sn','=',$fightSn]];
        $maxRound=$this->getMax($where,'round');
        if($maxRound>0)
        {
            $currentWhere=[['fight_sn','=',$fightSn],['user_id','=',$uid],['round','=',$maxRound]];
            return $this->getModel()->where($currentWhere)->findOrEmpty();
        }else{
            return [];
        }
  
    }
    public function saveInfo($infoData){
        var_dump(11);
        $fightDao =app()->make(GameFightDao::class);
        $fightRow=$fightDao->getFightRow($infoData['fight_sn']);
        var_dump(12);
        if( !$fightRow->isEmpty()&&$fightRow['status']==2){
            $type=$fightDao->getUserType($infoData['fight_sn'],$infoData['user_id']);
            var_dump(13);
            if($type>0){
                $where=[['fight_sn','=',$infoData['fight_sn']],['user_id','=',$infoData['user_id']],['round','=',$infoData['round']]];
                $infoRow=$this->getModel()->where($where)->findOrEmpty();
                var_dump(14);
                $roundWhere=[['fight_sn','=',$infoData['fight_sn']],['status','=',2]];
                $maxRound=$this->getMax($roundWhere,'round');
                var_dump($maxRound);
                var_dump(15);
                if($infoRow->isEmpty()&&$infoData['round']==$maxRound+1&&$maxRound>=0){
                    var_dump(16);
                    $infoData['type']=$type;
                    $infoData['status']=1;
                    $resault=$this->getModel()->create($infoData);
                    var_dump(17);
                    if($resault!==false){
                        var_dump(18);
                        $result=$this->checkResult($infoData['fight_sn']);
                        var_dump(19);
                        $resultData['status']=1;
                        $resultData['result_status']=$result['status'];
                    }else{
                        var_dump(20);
                        $resultData['status']=2;
                    }
                }else{
                    $resultData['status']=3;
                }
            }else{
                $resultData['status']=4;
            }
        }else{
            $resultData['status']=5; 
        }
        return $resultData;
    }

    //判定输赢
    public function checkResult($fightSn){
        $where=[['fight_sn','=',$fightSn],['status','=',1]];
        $count=$this->getModel()->where($where)->count();
        if($count==2){
            $list=$this->getModel()->where($where)->select()->toArray();
            $sum=$list[0]['gesture']+$list[1]['gesture'];
            $isNotVictory=true;
            $fightDao =app()->make(GameFightDao::class);
            if($list[0]['watchword']==$list[1]['watchword']){
                $fightDao->draw($fightSn);
                $this->getModel()->where($where)->update(['result'=>0]);
            }else{
                foreach($list as $ve){
                    if($ve['watchword']==$sum){
                        $scoreDao =app()->make(GameScoreDao::class);
                        $scoreDao->saveVictoryScore($ve['user_id']);
                        if($ve['type']==1){
                            $fightDao->victoryA($fightSn);
                            $isNotVictory=false;
                            foreach($list as $vr){
                                if($vr['id']==$ve['id']){
                                    $sonWhere=[['id','=',$vr['id']]];
                                    $this->getModel()->where($sonWhere)->update(['result'=>1]);
                                }else{
                                    $sonWhere=[['id','=',$vr['id']]];
                                    $this->getModel()->where($sonWhere)->update(['result'=>-1]);
                                }
                            }
                        }else{
                            $fightDao->victoryB($fightSn);
                            $isNotVictory=false;
                            foreach($list as $vr){
                                if($vr['id']==$ve['id']){
                                    $sonWhere=[['id','=',$vr['id']]];
                                    $this->getModel()->where($sonWhere)->update(['result'=>1]);
                                }else{
                                    $sonWhere=[['id','=',$vr['id']]];
                                    $this->getModel()->where($sonWhere)->update(['result'=>-1]);
                                }
                            }
                        }
                    }    
                }
                if($isNotVictory){
                    $fightDao->draw($fightSn);
                    $this->getModel()->where($where)->update(['result'=>0]);
                }
            }
          
            $this->getModel()->where($where)->update(['status'=>2]);
            return ['status'=>1];
        }else{
            return ['status'=>0];
        }
    }
    public function getInfoRow($fightSn,$userId,$round){
        $where=[['fight_sn','=',$fightSn],['user_id','=',$userId],['round','=',$round]];
        return $this->getModel()->where($where)->findOrEmpty();
    }
}

