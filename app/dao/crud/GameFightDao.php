<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * fight
 * @author crud自动生成代码
 * @date 2023/12/22 10:24:45
 */

namespace app\dao\crud;


use app\dao\BaseDao;
use app\model\crud\GameFight;
use app\services\user\UserServices;
use app\dao\crud\GameInfoDao;
/**
 * Class GameFightDao
 * @date 2023/12/22
 * @package app\dao\crud
 */
class GameFightDao extends BaseDao
{

    /**
     * 设置模型
     * @return string
     * @date 2023/12/22
     */
    protected function setModel(): string
    {
        return GameFight::class;
    }
    /**
     * 搜索
     * @param array $where
     * @return \crmeb\basic\BaseModel
     * @throws \ReflectionException
     * @date {%DATE%}
     */
    public function searchCrudModel(array $where = [], $field = ['*'], string $order = '', array $with = [])
    {
        return $this->getModel()->field($field)->when($order !== '', function ($query) use ($order) {
            $query->order($order);
        })->when($with, function ($query) use ($with) {
            $query->with($with);
        })->when(!empty($where['name']), function($query) use ($where) {
            $query->whereLike('name', '%'.$where['name'].'%');
        });
    }
    //获取当前对战状态
    public function getFightStatus(){
        $where[]=['status','<',3];
        $fightRow=$this->getModel()->where($where)->findOrEmpty();
        if(!$fightRow->isEmpty()&&$fightRow['status']==2){
            $resultData['status']=2;
            $services = app()->make(UserServices::class);
            $fightRow['user_a_info']=$services->read($fightRow['user_a'])['ps_info'];
            $fightRow['user_b_info']=$services->read($fightRow['user_b'])['ps_info'];
            $servicesInfo = app()->make(GameInfoDao::class);
            $fightRow['user_a_result']=$servicesInfo ->getInfoResult($fightRow['fight_sn'],$fightRow['user_a']);
            $fightRow['user_b_result']=$servicesInfo ->getInfoResult($fightRow['fight_sn'],$fightRow['user_b']);
            $resultData['fight_row']=$fightRow;
        }else if(!$fightRow->isEmpty()&&$fightRow['status']==1){
            $resultData['status']=1;
            $services = app()->make(UserServices::class);
            $fightRow['user_a_info']=$services->read($fightRow['user_a'])['ps_info'];
            $fightRow['user_b_info']=[];
            $resultData['fight_row']=$fightRow;
        }else{
            $resultData['status']=0;
        }
        return $resultData;
    }
    public function createFight($uid){
        $baseWhere=[['status','=',2]];
        $baseRow=$this->getModel()->where($baseWhere)->findOrEmpty();
        if($baseRow->isEmpty()){
            $where=[['status','=',1]];
            $fightRow=$this->getModel()->where($where)->findOrEmpty();
            if($fightRow->isEmpty()){
                $count=time()%9999;
                $fightData['name']=date("m月d日")." 第".$count."场";
                $fightData['fight_sn']=date("ymd"). $count;
                $fightData['user_a']=$uid;
                $fightData['user_b']=0;
                $fightData['draw']=$fightData['victory_b']=$fightData['victory_a']=0;
                $fightData['status']=1;
                $this->getModel()->create($fightData);
            }else{
                $fightData['fight_sn']=$fightRow['fight_sn'];
                if($fightRow['user_a']!=$uid){
                    $fightData['user_b']=$uid;
                    $fightData['status']=2;
                    $this->getModel()->where($where)->update($fightData);
                }
            }
            $newWhere=[['fight_sn','=',$fightData['fight_sn']]];
            $resultData['status']=1;
            $resultData['data']=$this->getModel()->where($newWhere)->findOrEmpty();
            return  $resultData;
        }else{
            $resultData['status']=0;
            $resultData['data']=$baseRow;
            return $resultData;
        }
       
    }
    public function getUserType($fightSn,$uid){
        $baseWhere=[['fight_sn','=',$fightSn]];
        $baseRow=$this->getModel()->where($baseWhere)->findOrEmpty();
        if($baseRow['user_a']==$uid){
            return 1;
        }else if($baseRow['user_b']==$uid){
            return 2;
        }else{
            return 0;
        }
    }
    public function victoryA($fightSn){
        $baseWhere=[['fight_sn','=',$fightSn]];
        return $this->getModel()->where($baseWhere)->inc('victory_a',1)->update();
    }
    public function victoryB($fightSn){
        $baseWhere=[['fight_sn','=',$fightSn]];
        return $this->getModel()->where($baseWhere)->inc('victory_b')->update();
    }
    public function draw($fightSn){
        $baseWhere=[['fight_sn','=',$fightSn]];
        return $this->getModel()->where($baseWhere)->inc('draw')->update();
    }
    public function getFightRow($fightSn){
        $baseWhere=[['fight_sn','=',$fightSn]];
        return $this->getModel()->where($baseWhere)->findOrEmpty();
    }

    public function endFightRow($fightSn){
        $baseWhere=[['fight_sn','=',$fightSn]];
        return $this->getModel()->where($baseWhere)->update(['status'=>3]);
    }
    public function endUserFightRow($uid){
        $baseWhere=[['user_a|user_b','=',$uid]];
        
        $result=$this->getModel()->where($baseWhere)->update(['status'=>3]);
        var_dump($this->getModel()->getLastSql());
        return $result;
    }
}
