<?php
/**
 *  +----------------------------------------------------------------------
 *  | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 *  +----------------------------------------------------------------------
 *  | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
 *  +----------------------------------------------------------------------
 *  | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 *  +----------------------------------------------------------------------
 *  | Author: CRMEB Team <admin@crmeb.com>
 *  +----------------------------------------------------------------------
 */

/**
 * game_score
 * @author crud自动生成代码
 * @date 2023/12/22 10:45:44
 */

namespace app\dao\crud;


use app\dao\BaseDao;
use app\model\crud\GameScore;
use app\services\activity\coupon\StoreCouponIssueServices;

/**
 * Class GameScoreDao
 * @date 2023/12/22
 * @package app\dao\crud
 */
class GameScoreDao extends BaseDao
{

    /**
     * 设置模型
     * @return string
     * @date 2023/12/22
     */
    protected function setModel(): string
    {
        return GameScore::class;
    }
    /**
     * 搜索
     * @param array $where
     * @return \crmeb\basic\BaseModel
     * @throws \ReflectionException
     * @date {%DATE%}
     */
    public function searchCrudModel(array $where = [], $field = ['*'], string $order = '', array $with = [])
    {
        return $this->getModel()->field($field)->when($order !== '', function ($query) use ($order) {
            $query->order($order);
        })->when($with, function ($query) use ($with) {
            $query->with($with);
        })->when(!empty($where['user_id']), function($query) use ($where) {
            $query->where('user_id', '=', $where['user_id']);
        })->when(!empty($where['day']), function($query) use ($where) {
            $query->where('day', '=', $where['day']);
        })->when(!empty($where['month']), function($query) use ($where) {
            $query->where('month', '=', $where['month']);
        })->when(!empty($where['year']), function($query) use ($where) {
            $query->where('year', '=', $where['year']);
        });
    }
    public function getUserScore($uid){
        var_dump("用户积分");
        var_dump($uid);
        $sroceRow=$this->getModel()->where('user_id','=',$uid)->findOrEmpty();
        var_dump($sroceRow->isEmpty());
        if($sroceRow->isEmpty()){
            $saveData=[
                'user_id'=>$uid,
                'day'=>0,
                'month'=>0,
                'year'=>0
            ];
            var_dump($saveData);
            $this->getModel()->insert($saveData);
            var_dump(9999);
            $sroceRow=$this->getModel()->where('user_id','=',$uid)->findOrEmpty();
        }
        $resultData['user_id']=$sroceRow['user_id'];
        $resultData['day']=$sroceRow['day'];
        $resultData['month']=$sroceRow['month'];
        $resultData['year']=$sroceRow['year'];
        $where[]=['id','>',0];
        $resultData['max_day']=$this->getMax($where,'day');
        $resultData['max_month']=$this->getMax($where,'month');
        $resultData['max_year']=$this->getMax($where,'year');
        return $resultData;
    }
    public function saveVictoryScore($uid){
        $this->getModel()->where('user_id','=',$uid)->inc('day',1)->update();
        $this->getModel()->where('user_id','=',$uid)->inc('month',1)->update(); 
        $this->getModel()->where('user_id','=',$uid)->inc('year',1)->update();
    }
    public function getMaxScore(){
        $maxDay=$this->getMax([['id','>',0]],'day');
        $resultData['max_day']=$maxDay;
        if($maxDay>0){
            $sroceDayList=$this->getModel()->order("day","desc")->limit(3)->select();
            $resultData['day_list']=$sroceDayList;
            if(!empty($sroceDayList)){
                foreach($sroceDayList as $ve){
                    $this->sendCoupon(5, $ve['user_id']);
                }
                $this->getModel()->update(['day'=>0],[['id','>',0]]);
            }
        
        }
        if(date("d")==1){
            $maxMonth=$this->getMax([['id','>',0]],'month');
            $resultData['max_month']=$maxMonth;
            if($maxMonth>0){
                $monthWhere=[['month','=',$maxMonth]];
                $sroceRow=$this->getModel()->where($monthWhere)->findOrEmpty();
                $resultData['month_row']=$sroceRow;
                if(!$sroceRow->isEmpty()&&$sroceRow['user_id']>0){
                    $this->sendCoupon(6, $sroceRow['user_id']);
                }
                $this->getModel()->update(['month'=>0],[['id','>',0]]);
            }
        }
        if(date("d")==1&&date("m")==1){
            $maxYear=$this->getMax([['id','>',0]],'year');
            $resultData['max_year']=$maxYear;
            if($maxYear>0){
                $yearWhere=[['year','=',$maxYear]];     
                $sroceRow=$this->getModel()->where($yearWhere)->findOrEmpty();
                $resultData['year_row']=$sroceRow;
                if(!$sroceRow->isEmpty()&&$sroceRow['user_id']>0){
                    $this->sendCoupon(7, $sroceRow['user_id']);
                }
                $yearWhere=[['year','<',$maxYear]];
                $sroceline=$this->getModel()->where($yearWhere)->order("year","desc")->findOrEmpty();
                $resultData['year2_row']=$sroceline;
                if(!$sroceline->isEmpty()&&$sroceline['user_id']>0){
                    $this->sendCoupon(8, $sroceline['user_id']);
                }
                $this->getModel()->update(['year'=>0],[['id','>',0]]);
           }
        }
        file_put_contents("coupon.log",date("Y-m-d H:i:s").PHP_EOL.json_encode( $resultData).PHP_EOL.PHP_EOL,FILE_APPEND);
        return $resultData;
    }
    public function sendCoupon($couponId,$userId){
        $issueService = app()->make(StoreCouponIssueServices::class);
        $coupon = $issueService->get($couponId);
        $issueService->setCoupon($coupon, [$userId]);
    }
}
