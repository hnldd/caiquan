<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace crmeb\services\workerman\game;

use app\services\crud\GameFightServices;
use app\services\crud\GameInfoServices;
use app\dao\crud\GameFightDao;
use Channel\Client;
use crmeb\services\workerman\ChannelService;
use crmeb\services\workerman\Response;
use Workerman\Connection\TcpConnection;
use Workerman\Lib\Timer;
use Workerman\Worker;

class GameService
{
    /**
     * @var Worker
     */
    protected $worker;

    /**
     * @var TcpConnection[]
     */
    protected $connections = [];

    /**
     * @var TcpConnection[]
     */
    protected $user = [];

    /**
     * 在线客服
     * @var TcpConnection[]
     */
    protected $kefuUser = [];

    /**
     * @var GameHandle
     */
    protected $handle;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var int
     */
    protected $timer;

    public function __construct(Worker $worker)
    {
        $this->worker = $worker;
        $this->handle = new GameHandle($this);
        $this->response = new Response();
    }

    public function onConnect(TcpConnection $connection)
    {
        $this->connections[$connection->id] = $connection;
        $connection->lastMessageTime = time();
        
    }
    public function setUser(TcpConnection $connection)
    {
        $this->user[$connection->user->uid] = $connection;
    }
    public function onMessage(TcpConnection $connection, $res)
    {
        $connection->lastMessageTime = time();
        $res = json_decode($res, true);
        if (!$res || !isset($res['type']) || !$res['type'] || $res['type'] == 'ping') {
            return $this->response->connection($connection)->success('ping', ['now' => time(),'msg'=>"游戏连接成功"]);
        }
        var_dump( $res['type']);
        if (!method_exists($this->handle, $res['type'])) {
            return $this->response->connection($connection)->success('error', ['now' => time(),'msg'=>"没有找到这个方法"]);
        }
        try {
            $this->handle->{$res['type']}($connection, $res + ['data' => []], $this->response->connection($connection));
        } catch (\Throwable $e) {
        }
    }


    public function onWorkerStart(Worker $worker)
    {
        ChannelService::connet();
        $this->timer = Timer::add(1500, function () use (&$worker) {
            $time_now = time();
            foreach ($worker->connections as $connection) {
                if ($time_now - $connection->lastMessageTime > 1200) {
                    $this->response->connection($connection)->close('timeout');
                }
            }
        });
    }


    public function onClose(TcpConnection $connection)
    {
        unset($this->connections[$connection->id]);
        if (isset($connection->user->uid)) {
            $fightDao = app()->make(GameFightDao::class);
            $endStatus=$fightDao ->endUserFightRow($connection->user->uid);
            unset($this->user[$connection->user->uid]);
        }
    }
    public function sendAllUserMsg(){
        foreach($this->user as $k=>$ve){
            $this->handle->getFightStatus($ve, [], $this->response->connection($ve));
        }
    }
    public function sendResultAll($fightSn,$round){
        $services = app()->make(GameFightServices::class);
        $fightRow= $services->getFightRow($fightSn);
        $servicesInfo = app()->make(GameInfoServices::class);
        $userAResult=$servicesInfo ->getInfoRow($fightSn,$fightRow['user_a'],$round);
        $userBResult=$servicesInfo ->getInfoRow($fightSn,$fightRow['user_b'],$round);
        $this->response->connection($this->user[$fightRow['user_a']])->success('result',  $userAResult);
        $this->response->connection($this->user[$fightRow['user_b']])->success('result',  $userBResult);
    }
}
