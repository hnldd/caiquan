<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

namespace crmeb\services\workerman\game;
use app\services\user\UserServices;
use app\services\user\UserAuthServices;
use app\services\crud\GameScoreServices;
use app\services\crud\GameFightServices;
use app\services\crud\GameInfoServices;
use crmeb\exceptions\AuthException;
use crmeb\services\app\WechatService;
use crmeb\services\workerman\Response;
use crmeb\utils\Arr;
use think\facade\Log;
use Workerman\Connection\TcpConnection;

/**
 * Class GameHandle
 * @package crmeb\services\workerman\chat
 */
class GameHandle
{
    /**
     * @var ChatService
     */
    protected $service;

    /**
     * ChatHandle constructor.
     * @param GameService $service
     */
    public function __construct(GameService &$service)
    {
        $this->service = &$service;
    }

   

    /**
     * 用户登录
     * @param TcpConnection $connection
     * @param array $res
     * @param Response $response
     * @return bool|null
     */
    public function login(TcpConnection &$connection, array $res, Response $response)
    {
        if (!isset($res['data']) || !$token = $res['data']) {
            return $response->close([
                'msg' => '授权失败!'
            ]);
        }
        try {
            /** @var UserAuthServices $services */
            $services = app()->make(UserAuthServices::class);
            $authInfo = $services->parseToken($token);
        } catch (AuthException $e) {
            return $response->close([
                'msg' => $e->getMessage()
            ]);
        }
        var_dump(3);
        $connection->user = $authInfo['user'];
        $connection->tokenData = $authInfo['tokenData'];
        var_dump(4);
        $this->service->setUser($connection);
        var_dump(5);
        $newData['user_id']=$authInfo['user']['uid'];
        $newData['avatar']=$authInfo['user']['avatar'];
        $newData['nickname']=$authInfo['user']['nickname'];
        $newData['real_name']=$authInfo['user']['real_name'];
        $resData['data']['uid']=$authInfo['user']['uid'];
        var_dump(6);
        $this->getUserScore($connection,$resData,$response);
        var_dump(7);
        $resultData['status']=1;
        $resultData['data']=$newData;
        $response->connection($connection)->success("login_info", $resultData);
        $connection->lastMessageTime = time();
        var_dump(8);
        $this->getFightStatus($connection,[],$response);
        var_dump(9);
    }
    //获取当前对局状态
    public function getFightStatus(TcpConnection &$connection,array $res, Response $response){
        $services = app()->make(GameFightServices::class);
        $response->connection($connection)->success("fight_staus", $services->getFightStatus());
        $connection->lastMessageTime = time();
    }
    //获取用户对战积分
    public function getUserScore(TcpConnection &$connection,array $res, Response $response){
        $services = app()->make(GameScoreServices::class);
        $response->connection($connection)->success("user_score", $services->getUserSroce($res['data']['uid']));
        $connection->lastMessageTime = time();
    }
    //参加对局
    public function createFight(TcpConnection &$connection,array $res, Response $response){
        $services = app()->make(GameFightServices::class);
        $result=$services->createFight($res['data']['uid']);
        if(isset($result['status'])&&$result['status']==1){
            $this->service->sendAllUserMsg();
        }
        $response->connection($connection)->success("fight_create", $result);
        $connection->lastMessageTime = time();
    }
    //出拳
    public function saveInfo(TcpConnection &$connection,array $res, Response $response){
        $services = app()->make(GameInfoServices::class);
        $infoData=$services->saveInfo($res['data']);
        $response->connection($connection)->success("save_info", $infoData);
        if(isset($infoData['result_status'])&&$infoData['result_status']==1){
            $this->service->sendAllUserMsg();
        }
    }
    //结束对局
    public function endFightRow(TcpConnection &$connection,array $res, Response $response){
        $services = app()->make(GameFightServices::class);
        $response->connection($connection)->success("fight_end", $services->endFightRow($res['data']['fight_sn']));
    }
}
