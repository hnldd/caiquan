// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import request from '@/libs/request';

/**
 * 获取列表数据
 * @param params
 * @return {*}
 */
export function getGameScoreListApi(params) {
    return request({
        url: 'crud/game_score',
        method: 'get',
        params,
    });
}

/**
 * 获取添加表单数据
 * @return {*}
 */
export function getGameScoreCreateApi() {
    return request({
        url: 'crud/game_score/create',
        method: 'get',
    });
}

/**
 * 添加数据
 * @param data
 * @return {*}
 */
export function gameScoreSaveApi(data) {
    return request({
        url: 'crud/game_score',
        method: 'post',
        data
    });
}

/**
 * 获取编辑表单数据
 * @param id
 * @return {*}
 */
export function getGameScoreEditApi(id) {
    return request({
        url: `crud/game_score/${id}/edit`,
        method: 'get'
    });
}

/**
 * 修改数据
 * @param id
 * @return {*}
 */
export function gameScoreUpdateApi(id, data) {
    return request({
        url: `crud/game_score/${id}`,
        method: 'put',
        data
    });
}

/**
 * 修改状态
 * @param id
 * @return {*}
 */
export function gameScoreStatusApi(id, data) {
    return request({
        url: `crud/game_score/status/${id}`,
        method: 'put',
        data
    });
}

/**
 * 删除数据
 * @param id
 * @return {*}
 */
export function gameScoreDeleteApi(id) {
    return request({
        url: `crud/game_score/${id}`,
        method: 'delete'
    });
}

/**
 * 获取数据
 * @param id
 * @return {*}
 */
export function getGameScoreReadApi(id) {
    return request({
        url: `crud/game_score/${id}`,
        method: 'get'
    });
}


