// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

import request from '@/libs/request';

/**
 * 获取列表数据
 * @param params
 * @return {*}
 */
export function getGameFightListApi(params) {
    return request({
        url: 'crud/game_fight',
        method: 'get',
        params,
    });
}

/**
 * 获取添加表单数据
 * @return {*}
 */
export function getGameFightCreateApi() {
    return request({
        url: 'crud/game_fight/create',
        method: 'get',
    });
}

/**
 * 添加数据
 * @param data
 * @return {*}
 */
export function gameFightSaveApi(data) {
    return request({
        url: 'crud/game_fight',
        method: 'post',
        data
    });
}

/**
 * 获取编辑表单数据
 * @param id
 * @return {*}
 */
export function getGameFightEditApi(id) {
    return request({
        url: `crud/game_fight/${id}/edit`,
        method: 'get'
    });
}

/**
 * 修改数据
 * @param id
 * @return {*}
 */
export function gameFightUpdateApi(id, data) {
    return request({
        url: `crud/game_fight/${id}`,
        method: 'put',
        data
    });
}

/**
 * 修改状态
 * @param id
 * @return {*}
 */
export function gameFightStatusApi(id, data) {
    return request({
        url: `crud/game_fight/status/${id}`,
        method: 'put',
        data
    });
}

/**
 * 删除数据
 * @param id
 * @return {*}
 */
export function gameFightDeleteApi(id) {
    return request({
        url: `crud/game_fight/${id}`,
        method: 'delete'
    });
}

/**
 * 获取数据
 * @param id
 * @return {*}
 */
export function getGameFightReadApi(id) {
    return request({
        url: `crud/game_fight/${id}`,
        method: 'get'
    });
}


