// +---------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +---------------------------------------------------------------------
// | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
// +---------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +---------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +---------------------------------------------------------------------

import LayoutMain from '@/layout';
import setting from '@/setting'

let routePre = setting.routePre

const meta = {
    auth: true,
}

const pre = 'game_fight_'

export default {
    path: `${routePre}`,
    name: 'crud_game_fight',
    header: '',
    meta,
    component: LayoutMain,
    children: [
        {
            path: 'crud/game_fight',
            name: `${pre}list`,
            meta: {
                auth: ['game_fight-crud-list-index'],
                title: '对局管理',
            },
            component: () => import('@/pages/crud/gameFight/index'),
        },
    ],
}
